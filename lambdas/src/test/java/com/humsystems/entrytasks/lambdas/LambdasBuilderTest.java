package com.humsystems.entrytasks.lambdas;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;

public class LambdasBuilderTest {

    private static Random random;

    private int getRandomNaturalNumber(){
        final int DEFAULT_MAX_BOUND = 100000;
        return getRandomNaturalNumber(DEFAULT_MAX_BOUND);
    }

    private int getRandomNaturalNumber(int boundInclusive){
        return 1 + random.nextInt(boundInclusive);
    }

    @BeforeClass
    public static void setUp(){
        long seed = System.currentTimeMillis();
        System.out.println("Randomizer initialized with seed " + seed);
        random = new Random(seed);
    }

    @Test(expected = NullPointerException.class)
    public void buildIsOdd_WhenNull_ThenNPEIsThrown() {
        IntLambdasBuilder.buildIsOdd().apply(null);
    }

    @Test
    public void buildIsOdd_WhenRandomOddGreaterThan1IsProvided_ThenItIsConsideredOdd() {
        int randomOdd = getRandomNaturalNumber() * 2 - 1;
        Assert.assertTrue(IntLambdasBuilder.buildIsOdd().apply(randomOdd));
    }


    @Test
    public void buildIsOdd_WhenRandomNegativeOddIsProvided_ThenItIsConsideredOdd() {
        int randomOdd = -(getRandomNaturalNumber() * 2 - 1);
        Assert.assertTrue(IntLambdasBuilder.buildIsOdd().apply(randomOdd));
    }


    @Test
    public void buildIsOdd_WhenRandomEvenIsProvided_ThenItIsNotConsideredOdd() {
        int randomEven = getRandomNaturalNumber() * 2;
        Assert.assertFalse("Number " + randomEven + "is considered odd", IntLambdasBuilder.buildIsOdd().apply(randomEven));
    }

    @Test
    public void buildIsOdd_WhenRandomNegativeEvenIsProvided_ThenItIsNotConsideredOdd() {
        int randomEven = -(getRandomNaturalNumber() * 2);
        Assert.assertFalse("Number " + randomEven + "is considered odd", IntLambdasBuilder.buildIsOdd().apply(randomEven));
    }


    @Test(expected = NullPointerException.class)
    public void buildIsPrime_WhenNull_ThenNPEIsThrown() {
        IntLambdasBuilder.buildIsPrime().apply(null);
    }

    @Test
    public void buildIsPrime_When0Supplied_ThenItIsNotConsideredPrime() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(0));
    }

    @Test
    public void buildIsPrime_When1Supplied_ThenItIsNotConsideredPrime() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(1));
    }

    @Test
    public void buildIsPrime_WhenPrimeSupplied_ThenItIsConsideredPrime_1() {
        Assert.assertTrue(IntLambdasBuilder.buildIsPrime().apply(2));
    }

    @Test
    public void buildIsPrime_WhenPrimeSupplied_ThenItIsConsideredPrime_2() {
        Assert.assertTrue(IntLambdasBuilder.buildIsPrime().apply(83));
    }

    @Test
    public void buildIsPrime_WhenPrimeSupplied_ThenItIsConsideredPrime_3() {
        Assert.assertTrue(IntLambdasBuilder.buildIsPrime().apply(310248241));
    }

    @Test
    public void buildIsPrime_WhenCompositeSupplied_ThenItIsNotConsideredPrime_1() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(8));
    }

    @Test
    public void buildIsPrime_WhenCompositeSupplied_ThenItIsNotConsideredPrime_2() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(63));
    }

    @Test
    public void buildIsPrime_WhenCompositeSupplied_ThenItIsNotConsideredPrime_3() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(169));
    }

    @Test
    public void buildIsPrime_WhenCompositeSupplied_ThenItIsNotConsideredPrime_4() {
        Assert.assertFalse(IntLambdasBuilder.buildIsPrime().apply(319086769)); // prime(2048) ^ 2 = 17863 ^ 2
    }


    @Test(expected = NullPointerException.class)
    public void buildIsPalindrome_WhenNullSupplied_ThenNPEIsThrown() {
        IntLambdasBuilder.buildIsPalindrome().apply(null);
    }

    @Test
    public void buildIsPalindrome_WhenRandomSingleDigitNumberIsSupplied_ThenItIsConsideredPalindrome() {
        IntLambdasBuilder.buildIsPalindrome().apply(getRandomNaturalNumber(10));
    }

    @Test
    public void buildIsPalindrome_WhenRandomDoubleDigitNumberNotDivisibleBy11IsGiven_ThenItIsNotConsideredPalindrome() {
        IntLambdasBuilder.buildIsPalindrome().apply(getRandomNaturalNumber(10) * 11 - 1);
    }

    @Test
    public void buildIsPalindrome_WhenEvenPalindromeIsSupplied_ThenItIsConsideredPalindrome() {
        IntLambdasBuilder.buildIsPalindrome().apply(1221);
    }

    @Test
    public void buildIsPalindrome_WhenOddPalindromeIsSupplied_ThenItIsConsideredPalindrome_1() {
        IntLambdasBuilder.buildIsPalindrome().apply(12521);
    }

    @Test
    public void buildIsPalindrome_WhenOddPalindromeIsSupplied_ThenItIsConsideredPalindrome_2() {
        IntLambdasBuilder.buildIsPalindrome().apply(24042);
    }

    @Test
    public void buildIsPalindrome_WhenPalindromeIsMultipliedByTen_ThenItIsNotConsideredPalindrome() {
        IntLambdasBuilder.buildIsPalindrome().apply(240420);
    }

    @Test
    public void buildIsPalindrome_WhenNotAPalindromeIsSupplied_ThenItIsNotConsideredPalindrome() {
        IntLambdasBuilder.buildIsPalindrome().apply(94798347);
    }
}