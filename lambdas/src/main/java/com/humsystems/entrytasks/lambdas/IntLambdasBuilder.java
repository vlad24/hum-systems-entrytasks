package com.humsystems.entrytasks.lambdas;

import java.util.function.Function;
import java.util.stream.IntStream;

public class IntLambdasBuilder {

    public static Function<Integer, Boolean> buildIsOdd(){
        return (Integer x) -> x % 2 != 0;
    }

    public static Function<Integer, Boolean> buildIsPrime(){
        return (Integer x) -> x > 1 && IntStream.rangeClosed(2, Double.valueOf(Math.sqrt(x)).intValue()).noneMatch(d -> x % d == 0);
    }


    public static Function<Integer, Boolean> buildIsPalindrome(){
        return (Integer x) -> {
            int digitAmount = Double.valueOf(Math.log10(x.doubleValue())).intValue(); // can be inlined, however extracted for readability
            return x == IntStream.rangeClosed(1, digitAmount)
                                .reduce(0, (acc, n) ->
                                        acc + ((x / tenPower(n)) % 10) * tenPower(digitAmount - n)
                                );
        };
    }

    /**
     * Computes power of ten.
     * Helper method. Can be inlined, but stands alone for readablility
     * @param power power to which 10 is raised
     * @return 10^power
     */
    private static Integer tenPower(int power) {
        return ((Double) Math.pow(10.0, (double) power)).intValue();
    }

}
