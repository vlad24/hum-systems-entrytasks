# HUM systems programming challenge. Task 2
## Task
This Java 8 challenge tests your knowledge of Lambda expressions.
Write the following methods that return a lambda expression performing a specified action:

* Perform Operation is Odd (): The lambda expression must return true if a number is odd, or
false if it is even
* Perform Operation is Prime (): The lambda expression must return true if a number is prime,
or false if it is composite
* Perform Operation is Palindrome (): The lambda expression must return true if a number is a
palindrome, or false if it is not

## Solution overview

Technically, my solution represents a maven project with just one dependency on junit4.

### Notes

Some lambdas rely on helper methods. 
Those methods are just created for readability.


### How to build/check application
Build:
```{bash}
    mvn package
```
Evaluate correctness:
```{bash}
    mvn test
```
 