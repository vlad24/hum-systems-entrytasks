# HUM systems programming challenge. Task 1
## Task
Write the following program and write unit tests for extra points.
The application should present a small questionnaire for the user to fill out. Finally, the questionnaire
is analysed and the result should be presented to the user. The communication with the (not existing)
server is to be implemented as a mock.

The questionnaire consists of 3 multiple choice questions. Each question should be queried in a
separate step. Each answer has points. The points of the chosen answer are added at the end. The
user should be able to jump back to the previous answer.
The example questions are:

Do you enjoy working in a team?
* Teamwork is in my blood (5 points)
* Yes, I do (3 points)
* I prefer to work alone (0 points)

How long have you been working with Java?
* Never (0 points)
* Less than 1 year (1 point)
* Less than 2 years (3 points)
* More than 3 years (5 points)

## Solution overview

Technically, my solution represents a maven project with just one dependency on junit4.

### Design

The solution is represented as an *QuestionnaireApplication* abstract class which
has a *run* method. One implementation of the interface is provided as *QuestionnaireAppConsoleImpl*.
The application itself consists of several parts called **components** which are queried by the application from its factory opject
represented as an instance *ComponentFactory* class.

The factory class makes it easy to parametrize the content of its QuestionnaireApplication and if some QuestionnaireApplication
needs to be somehow changed, no massive code changes required - just change the factory it uses.

The components themselves are more or less logically decoupled units building up the application logic.
The components are: Logger, Answerer, ReportSender, ReportEvaluator and ConfigurationProvider.
Changing one component within an app should not dramatically change the behaviour of other components.
Please refer to the documentation of these classes for more details. 

The most important and sophisticated among all the other components is an *Answerer* one.
This component has its unit tests under test folder.

### How to build/run application or just check it correctness
(assumed that you are currently located in the project root directory)
Build:
```{bash}
    mvn package
```
Run:
```{bash}
java -jar ./target/questionnaire-1.0-SNAPSHOT.jar
```
**Note**: last build is provided under target folder
or just use your favorite IDE ;)

Check correctness:
```{bash}
mvn test
```
 
