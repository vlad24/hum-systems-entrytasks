package com.humsystems.entrytasks.questionnaire.app.component;

import com.humsystems.entrytasks.questionnaire.domain.Questionnaire;

/**
 * Dependency injection container analog, representing giving flexible control over applications components it depends on
 */
public interface ComponentFactory {

    QuestionnaireAnswerer getQuestionnaireNavigator(String questionee, Questionnaire questionnaire);

    Logger getAppLogger();

    ReportSender getReportSender();

    ConfigurationsProvider getConfigurationProvider();

    ReportEvaluator getReportConfigurator();

}
