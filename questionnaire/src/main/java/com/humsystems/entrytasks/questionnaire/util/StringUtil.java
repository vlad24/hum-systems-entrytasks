package com.humsystems.entrytasks.questionnaire.util;

public class StringUtil {
    public static boolean isBlank(String string){
        return string == null || string.trim().equals("");
    }

}
