package com.humsystems.entrytasks.questionnaire.app.component;

import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReportInstantFeedback;

/**
 * Entity designed to provide the questionee the feedback regarding his answers
 */
public interface ReportEvaluator {
    QuestionnaireReportInstantFeedback buildFeedback(QuestionnaireReport report);
}
