package com.humsystems.entrytasks.questionnaire.app.component;

import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;

import java.io.IOException;
import java.net.URL;

public interface ReportSender {
    void sendReport(URL address, QuestionnaireReport report, int connectTimeout) throws IOException;
}
