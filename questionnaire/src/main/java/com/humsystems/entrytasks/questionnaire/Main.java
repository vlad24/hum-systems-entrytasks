package com.humsystems.entrytasks.questionnaire;

import com.humsystems.entrytasks.questionnaire.app.QuestionnaireApp;
import com.humsystems.entrytasks.questionnaire.app.QuestionnaireAppConsoleImpl;

public class Main {

    public static void main(String[] args) {
        QuestionnaireApp application = new QuestionnaireAppConsoleImpl();
        application.run();
    }
}
