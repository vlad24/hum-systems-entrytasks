package com.humsystems.entrytasks.questionnaire.app.component;


public interface Logger {

    enum LEVEL {
        ERROR,
        INFO,
        WARN,
        TRACE
    }

     void print(LEVEL level, String message);
}
