package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.QuestionnaireAnswerer;
import com.humsystems.entrytasks.questionnaire.domain.Answer;
import com.humsystems.entrytasks.questionnaire.domain.Question;
import com.humsystems.entrytasks.questionnaire.domain.Questionnaire;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Simple non thread-safe implementation of {@link QuestionnaireAnswerer} which lets user interact with it only when it is opened
 */
public class QuestionnaireAnswererBasicImpl extends QuestionnaireAnswerer {

    private final String questionee;
    private final List<Question> questionList;
    private final List<Set<Integer>> givenAnswers;
    private LocalDateTime startTimestamp;
    private LocalDateTime endTimestamp;
    private int currentQuestionNumber;
    private boolean isClosed;


    public QuestionnaireAnswererBasicImpl(String questionee, Questionnaire questionnaire) {
        super(questionnaire);
        List<Question> questionsToAsk = questionnaire.getQuestions();
        this.questionee = questionee;
        questionList = new ArrayList<>(questionsToAsk);
        givenAnswers = new ArrayList<>(questionsToAsk.size());
        IntStream.range(0, questionsToAsk.size()).forEach(i -> givenAnswers.add(Collections.emptySet()));
        currentQuestionNumber = -1;
        startTimestamp = null;
        endTimestamp = null;
        isClosed = true;
    }


    @Override
    public void openAnswerSession() {
        isClosed = false;
        startTimestamp = LocalDateTime.now();
    }


    @Override
    protected int fetchCurrentQuestionNumber() {
        return currentQuestionNumber;
    }


    @Override
    public int getAllQuestionAmount() {
        return questionList.size();
    }


    @Override
    protected Optional<Question> fetchNext() {
        if (currentQuestionNumber < questionList.size() - 1){
            currentQuestionNumber++;
            return Optional.of(questionList.get(currentQuestionNumber));
        } else {
            currentQuestionNumber = questionList.size();
            return Optional.empty();
        }
    }


    @Override
    protected Optional<Question> fetchPrevious() {
        if (currentQuestionNumber > 0){
            currentQuestionNumber--;
            return Optional.of(questionList.get(currentQuestionNumber));
        } else {
            currentQuestionNumber = -1;
            return Optional.empty();
        }
    }


    @Override
    protected Optional<Question> fetchCurrent() {
        if (currentQuestionNumber >= 0 && currentQuestionNumber < questionList.size()) {
            return Optional.of(questionList.get(currentQuestionNumber));
        } else {
            return Optional.empty();
        }
    }


    @Override
    protected void answerCurrentlyOpenedQuestion(Set<Integer> answerPositions) {
        givenAnswers.set(currentQuestionNumber, answerPositions);
    }


    @Override
    protected Optional<Set<Integer>> fetchProvidedAnswers(int questionNumber) {
        Set<Integer> answerPositions = givenAnswers.get(questionNumber);
        return (answerPositions == null || answerPositions.isEmpty()) ? Optional.empty() : Optional.of(answerPositions);
    }


    @Override
    public boolean isCurrentQuestionAlreadyAnswered() {
        return !givenAnswers.get(currentQuestionNumber).isEmpty();
    }


    @Override
    public QuestionnaireReport closeAnswerSession(){
        endTimestamp = LocalDateTime.now();
        isClosed = true;
        return new QuestionnaireReport(startTimestamp, endTimestamp, questionee, computeFinalScore());
    }


    @Override
    public boolean isAnswerSessionOpened() {
        return !isClosed;
    }


    private int computeFinalScore() {
        int totalScore = 0;
        for (int i = 0; i < givenAnswers.size(); i++) {
            Question question = questionList.get(i);
            List<Answer> questionAnswers = question.getPossibleAnswers();
            Set<Integer> candidateAnswerNumbers = givenAnswers.get(i);
            int questionScore = IntStream.range(0, questionAnswers.size())
                    .filter(candidateAnswerNumbers::contains)
                    .map(answerNumber -> questionAnswers.get(answerNumber).getPoints())
                    .sum();
            totalScore += questionScore;
        }
        return totalScore;

    }




}
