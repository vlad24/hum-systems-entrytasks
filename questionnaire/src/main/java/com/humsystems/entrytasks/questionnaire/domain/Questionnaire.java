package com.humsystems.entrytasks.questionnaire.domain;


import java.util.Collections;
import java.util.List;

public class Questionnaire {

    private final String name;
    private final List<Question> questionsToAsk;


    public Questionnaire(String name, List<Question> questionsToAsk) {
        this.name = name;
        this.questionsToAsk = Collections.unmodifiableList(questionsToAsk);
    }


    public List<Question> getQuestions() {
        return questionsToAsk;
    }


    public String getName() {
        return name;
    }
}