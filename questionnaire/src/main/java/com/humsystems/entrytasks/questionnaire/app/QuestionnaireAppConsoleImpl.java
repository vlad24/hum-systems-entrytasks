package com.humsystems.entrytasks.questionnaire.app;

import com.humsystems.entrytasks.questionnaire.app.component.ComponentFactory;
import com.humsystems.entrytasks.questionnaire.app.component.ConfigurationsProvider.PREDEFINED_CONFIGS;
import com.humsystems.entrytasks.questionnaire.app.component.Logger;
import com.humsystems.entrytasks.questionnaire.app.component.QuestionnaireAnswerer;
import com.humsystems.entrytasks.questionnaire.app.component.impl.ComponentFactoryForConsoleImpl;
import com.humsystems.entrytasks.questionnaire.domain.*;

import java.net.URL;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.humsystems.entrytasks.questionnaire.app.component.Logger.LEVEL.*;

public class QuestionnaireAppConsoleImpl extends QuestionnaireApp {

    private final Scanner inputScanner;
    private ComponentFactory appComponentFactory;
    private final Questionnaire questionnaire;
    private QuestionnaireAnswerer answerer;
    private String candidate;
    private STATE state;
    private final Logger outputPrinter;


    public QuestionnaireAppConsoleImpl() {
        super();
        inputScanner = new Scanner(System.in);
        outputPrinter = appLogger;
        state = STATE.MAIN;
        candidate = null;
        questionnaire = getQuestionnaire();
        answerer = null;
    }


    @Override
    protected ComponentFactory getComponentInitializerFactory() {
        if (appComponentFactory == null){
            appComponentFactory = new ComponentFactoryForConsoleImpl();
        }
        return appComponentFactory;
    }


    private Questionnaire getQuestionnaire() {
        return new Questionnaire("Hum systems introduction questionnaire",  Arrays.asList(
                new Question("Do you enjoy working in a team?", Arrays.asList(
                        new Answer("Teamwork is in my blood", 5),
                        new Answer("Yes, I do", 3),
                        new Answer("I prefer to work alone", 0)
                    ),
                        false),
                new Question("How long have you been working with Java?", Arrays.asList(
                        new Answer("Never", 0),
                        new Answer("Less than 1 year", 1),
                        new Answer("Less than 3 years", 3),
                        new Answer("Less than 5 years", 5)
                    ),
                        false),
                new Question("How do you feel about automated tests??", Arrays.asList(
                        new Answer("Mandatory", 3),
                        new Answer("Waste of time", 0)
                    ),
                        false)
            )
        );
    }


    @Override
    public void run() {
        greet();
        boolean needToStop = false;
        do {
            String userCommandInput = readUserInput(String.format("Enter command ('%s' for help): ", CONSOLE_COMMAND.HELP.getCode()));
            CONSOLE_COMMAND command = CONSOLE_COMMAND.getFor(userCommandInput.trim());
            switch (state) {
                case MAIN:
                    switch (command) {
                        case START_ANSWERING:
                            candidate = readUserInput(String.format("To start answering '%s', please introduce yourself: ", questionnaire.getName()));
                            answerer = appComponentFactory.getQuestionnaireNavigator(candidate, questionnaire);
                            state = STATE.ANSWERING;
                            answerer.openAnswerSession();
                            printNextQuestion();
                            break;
                        case HELP:
                            printHelpMessage();
                            break;
                        case QUIT:
                            needToStop = askForActionConfirmation(CONSOLE_COMMAND.QUIT);
                            break;
                        default:
                            outputPrinter.print(ERROR, "Incorrect or inappropriate command for normal mode");
                    }
                    break;
                case ANSWERING:
                    switch (command) {
                        case ANSWER_CURRENT_QUESTION:
                            readAndTryAnswer();
                        case NEXT_QUESTION:
                            printNextQuestion();
                            break;
                        case PREVIOUS_QUESTION:
                            printPreviousQuestion();
                            break;
                        case HELP:
                            printHelpMessage();
                            break;
                        case FINISH_ANSWERING:
                            boolean needToFinish = askForActionConfirmation(CONSOLE_COMMAND.FINISH_ANSWERING);
                            if (needToFinish) {
                                QuestionnaireReport questionnaireReport = answerer.closeAnswerSession();
                                sendReportAsync(questionnaireReport);
                                printReport(questionnaireReport);
                                printFeedback(reportEvaluator.buildFeedback(questionnaireReport));
                            }
                            needToStop = needToFinish;
                            break;
                        case QUIT:
                            needToStop = askForActionConfirmation(CONSOLE_COMMAND.QUIT);
                            break;
                        default:
                            outputPrinter.print(ERROR, "Incorrect or inappropriate command for answering mode");
                    }
            }
        } while (!needToStop);
        sayGoodbye();
    }


    private boolean askForActionConfirmation(CONSOLE_COMMAND command) {
        outputPrinter.print(WARN, "Are you sure you would like to " + command.getDescription() + "? (y/N)");
        return inputScanner.next().trim().equalsIgnoreCase("y");

    }


    private String readUserInput(String prompt) {
        outputPrinter.print(INFO, prompt);
        return inputScanner.next().trim().toLowerCase();
    }


    private void readAndTryAnswer() {
        String answer = readUserInput("Enter the comma-separated positions (e.g. 1,0,2): ");
        Optional<Set<Integer>> positions = parseAnswerPositionsFromInput(answer);
        if (positions.isPresent()) {
            try {
                answerer.answerCurrent(positions.get());
            } catch (IllegalArgumentException e){
                outputPrinter.print(ERROR, "The question is single choice one. Please choose one");
            }
        } else {
            outputPrinter.print(ERROR,  "Incorrect input provided, try again. Make sure your answers are comma separated");
        }
    }


    private void printPreviousQuestion() {
        Optional<Question> previousQuestion = answerer.getPrevious();
        if (previousQuestion.isPresent()) {
            printQuestionContent(previousQuestion.get());
        } else {
            outputPrinter.print(WARN, "No more questions available before. Move forward and review your answers");
        }
    }


    private void printNextQuestion() {
        Optional<Question> nextQuestion = answerer.getNext();
        if (nextQuestion.isPresent()) {
            printQuestionContent(nextQuestion.get());
        } else {
            outputPrinter.print(WARN, "No more questions available further. Finish your session");
        }
    }


    private void printQuestionContent(Question question) {
        int currentQuestionNumber = answerer.getCurrentQuestionNumber();
        StringBuilder builder = new StringBuilder("\n")
                .append("Question ")
                .append(1 + currentQuestionNumber).append("/").append(answerer.getAllQuestionAmount())
                .append(question.isMultipleChoice() ? "! multiple choice" : "! single choice")
                .append("\n")
                ;
        if (answerer.isCurrentQuestionAlreadyAnswered()){
            builder.append(" *already answered* ");
            builder.append("(").append(answerer.getProvidedAnswers(currentQuestionNumber)).append(")");
        }
        builder.append("\n");
        int answerPosition = 0;
        for (Answer answer : question.getPossibleAnswers()){
            builder.append(answerPosition++).append(") ").append(answer.getStatement()).append("\n");
        }
        outputPrinter.print(INFO, builder.toString());
    }


    private void printHelpMessage() {
        Arrays.stream(CONSOLE_COMMAND.values())
                .filter(c -> c != CONSOLE_COMMAND.UNKNOWN)
                .forEach(command -> outputPrinter.print(INFO,
                        String.format("%s (%s)\n\t:%s", command.getCode(), command.getLongCode(), command.getDescription())));
    }


    private void printReport(QuestionnaireReport questionnaireReport) {
        outputPrinter.print(INFO, String.format("Dear %s, you have finished your questionnaire in %s sec. Your score is %s.",
                questionnaireReport.getQuestionee(),
                questionnaireReport.getDuration().getSeconds(),
                questionnaireReport.getFinalScore()
        ));
    }


    private void printFeedback(QuestionnaireReportInstantFeedback instantFeedback) {
        outputPrinter.print(INFO, String.format("Your instant feedback: %s", instantFeedback.getComment()));
        outputPrinter.print(INFO, instantFeedback.isSatisfactory() ? "Congratulations!" : "We are very sorry, but do not worry!");
    }

    private Optional<Set<Integer>> parseAnswerPositionsFromInput(String answer) {
        try {
            Set<Integer> positions = Arrays.stream(answer.split(",")).map(String::trim).map(Integer::parseInt).collect(Collectors.toSet());
            return Optional.of(positions);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }


    private void greet() {
        outputPrinter.print(INFO, "*** Hum-Systems Entry Task. Old school simple questionnaire app by Vlad Pavlov");
    }


    private void sayGoodbye() {
        outputPrinter.print(INFO, "Thank you for your effort. See you soon!");
    }


    private void sendReportAsync(QuestionnaireReport report) {
        CompletableFuture.runAsync(() -> {
            try {
                final URL sendUrl = new URL(configurationsProvider.getStringProperty(PREDEFINED_CONFIGS.REPORT_SEND_URL.getPropKey()));
                final int connectTimeout = configurationsProvider.getIntegerProperty(PREDEFINED_CONFIGS.REPORT_SEND_TIMEOUT.getPropKey());
                outputPrinter.print(TRACE, String.format("Sending the report of %s to the server %s", report.getQuestionee(), sendUrl.toString()));
                reportSender.sendReport(sendUrl, report, connectTimeout);
                Thread.sleep(500);
                outputPrinter.print(TRACE, "Done with sending report");
            } catch (Exception e) {
                outputPrinter.print(ERROR, "Error occurred while sending " + e.getMessage());
            }
        });
    }


    private enum STATE {
        MAIN,
        ANSWERING
    }

    private enum CONSOLE_COMMAND {
        QUIT("q", "quit", "quit the application"),
        START_ANSWERING("s", "start", "start questionnaire"),
        NEXT_QUESTION(">", "next", "go to the next question"),
        PREVIOUS_QUESTION("<", "prev", "go to the previous question"),
        ANSWER_CURRENT_QUESTION("a", "answer", "answer current question"),
        FINISH_ANSWERING("f", "finish", "finish questionnaire"),
        HELP("?", "help", "display help message"),
        UNKNOWN(null, null, "unknown");

        CONSOLE_COMMAND(String code, String longCode, String description) {
            this.code = code;
            this.longCode = longCode;
            this.description = description;
        }

        private final String code;
        private String longCode;
        private String description;


        public static CONSOLE_COMMAND getFor(String input) {
            return Arrays.stream(CONSOLE_COMMAND.values())
                    .filter(command -> input.equalsIgnoreCase(command.getCode()) || input.equalsIgnoreCase(command.getLongCode()))
                    .findAny()
                    .orElse(UNKNOWN);
        }

        String getCode() {
            return code;
        }

        String getLongCode() {return longCode;}

        public String getDescription() {
            return description;
        }
    }

}
