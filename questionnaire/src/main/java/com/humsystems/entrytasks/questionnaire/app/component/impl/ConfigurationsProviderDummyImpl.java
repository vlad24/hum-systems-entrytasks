package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.ConfigurationsProvider;

import java.util.HashMap;
import java.util.Map;

/**
 * Dummy implementation of app configuration provider based on predefined values in a intrinsic map
 */
public class ConfigurationsProviderDummyImpl implements ConfigurationsProvider {

    private final Map<String, Object> props;


    public ConfigurationsProviderDummyImpl() {
        props = new HashMap<>();
        props.put("url.send_report", "http://127.0.0.1/report/");
        props.put("timeout.sec.send_report", 3);
        //so on
    }


    @Override
    public String getStringProperty(String name) {
        return String.valueOf(props.get(name));
    }


    @Override
    public Integer getIntegerProperty(String name) {
        return Integer.parseInt(String.valueOf(props.get(name)));
    }


    @Override
    public Double getDoubleProperty(String name) {
        return Double.parseDouble(String.valueOf(props.get(name)));
    }
}
