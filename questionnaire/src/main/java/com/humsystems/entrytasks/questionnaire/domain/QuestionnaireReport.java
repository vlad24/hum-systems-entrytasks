package com.humsystems.entrytasks.questionnaire.domain;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;

public class QuestionnaireReport {

    private final LocalDateTime startTimestamp;
    private final LocalDateTime endTimestamp;
    private final String questionee;
    private final int finalScore;


    public QuestionnaireReport(LocalDateTime startTimestamp, LocalDateTime endTimestamp, String answeringCandidate, int finalScore) {
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.questionee = answeringCandidate;
        this.finalScore = finalScore;
    }


    public LocalDateTime getStartTimestamp() {
        return startTimestamp;
    }


    public LocalDateTime getEndTimestamp() {
        return endTimestamp;
    }


    public String getQuestionee() {
        return questionee;
    }


    public int getFinalScore() {
        return finalScore;
    }

    public Duration getDuration(){
        return Duration.between(startTimestamp, endTimestamp);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionnaireReport that = (QuestionnaireReport) o;
        return finalScore == that.finalScore &&
                Objects.equals(startTimestamp, that.startTimestamp) &&
                Objects.equals(endTimestamp, that.endTimestamp) &&
                Objects.equals(questionee, that.questionee);
    }


    @Override
    public int hashCode() {
        return Objects.hash(startTimestamp, endTimestamp, questionee, finalScore);
    }


    @Override
    public String toString() {
        return "QuestionnaireReport{" +
                "startTimestamp=" + startTimestamp +
                ", endTimestamp=" + endTimestamp +
                ", questionee='" + questionee + '\'' +
                ", finalScore=" + finalScore +
                '}';
    }


}
