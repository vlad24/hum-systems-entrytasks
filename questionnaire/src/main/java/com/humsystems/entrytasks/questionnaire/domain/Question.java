package com.humsystems.entrytasks.questionnaire.domain;

import com.humsystems.entrytasks.questionnaire.util.StringUtil;

import java.util.List;
import java.util.Objects;

public class Question {

    private final String statement;
    private final List<Answer> possibleAnswers;
    private boolean isMultipleChoice;


    public Question(String statement, List<Answer> possibleAnswers, boolean isMultipleChoice) {
        validate(statement, possibleAnswers);
        this.statement = statement;
        this.possibleAnswers = possibleAnswers;
        this.isMultipleChoice = isMultipleChoice;
    }


    public String getStatement() {
        return statement;
    }


    public List<Answer> getPossibleAnswers() {
        return possibleAnswers;
    }


    public boolean isMultipleChoice() {
        return isMultipleChoice;
    }

    private void validate(String statement, List<Answer> possibleAnswers) {
        if (StringUtil.isBlank(statement)){
            throw new IllegalArgumentException("Cannot construct a question with empty statement");
        }
        if (possibleAnswers.isEmpty()){
            throw new IllegalArgumentException("Cannot construct a question with empty statement");
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return isMultipleChoice == question.isMultipleChoice &&
                Objects.equals(statement, question.statement) &&
                Objects.equals(possibleAnswers, question.possibleAnswers);
    }


    @Override
    public int hashCode() {

        return Objects.hash(statement, possibleAnswers, isMultipleChoice);
    }


    @Override
    public String toString() {
        return "Question{" +
                "statement='" + statement + '\'' +
                ", possibleAnswers=" + possibleAnswers +
                ", isMultipleChoice=" + isMultipleChoice +
                '}';
    }


}
