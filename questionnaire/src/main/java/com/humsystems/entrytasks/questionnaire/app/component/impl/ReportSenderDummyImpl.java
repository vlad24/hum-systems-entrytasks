package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.ReportSender;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;

import java.net.URL;

public class ReportSenderDummyImpl implements ReportSender {
    @Override
    public void sendReport(URL address, QuestionnaireReport report, int connectTimeout) {
        System.out.println("Report was sent to " + address + "\n");
    }


}
