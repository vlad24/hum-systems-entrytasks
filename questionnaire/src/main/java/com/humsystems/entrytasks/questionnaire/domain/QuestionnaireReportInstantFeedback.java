package com.humsystems.entrytasks.questionnaire.domain;

import java.util.Objects;

public class QuestionnaireReportInstantFeedback {

    private final boolean isSatisfactory;
    private final String comment;

    public QuestionnaireReportInstantFeedback(boolean isSatisfactory, String comment) {
        this.isSatisfactory = isSatisfactory;
        this.comment = comment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionnaireReportInstantFeedback that = (QuestionnaireReportInstantFeedback) o;
        return isSatisfactory == that.isSatisfactory &&
                Objects.equals(comment, that.comment);
    }


    @Override
    public int hashCode() {
        return Objects.hash(isSatisfactory, comment);
    }


    public boolean isSatisfactory() {
        return isSatisfactory;
    }


    public String getComment() {
        return comment;
    }
}
