package com.humsystems.entrytasks.questionnaire.domain;

import com.humsystems.entrytasks.questionnaire.util.StringUtil;

import java.util.Objects;

public class Answer {

    private String statement;
    private Integer points;


    public Answer(String statement, int points) {
        if (StringUtil.isBlank(statement)){
            throw new IllegalArgumentException("Answer statement cannot be empty!");
        }
        if (points < 0){
            throw new IllegalArgumentException("Points for answer statement cannot be negative!");
        }
        this.statement = statement;
        this.points = points;
    }


    public String getStatement() {
        return statement;
    }


    public Integer getPoints() {
        return points;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return Objects.equals(statement, answer.statement) &&
                Objects.equals(points, answer.points);
    }


    @Override
    public int hashCode() {

        return Objects.hash(statement, points);
    }


    @Override
    public String toString() {
        return "Answer{" +
                "statement='" + statement + '\'' +
                ", points=" + points +
                '}';
    }


}
