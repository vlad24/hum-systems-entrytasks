package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.Logger;

public class LoggerConsoleImpl implements Logger {
    @Override
    public void print(LEVEL level, String message) {
        switch (level){
            case INFO:
                System.out.println(message);
                break;
            case WARN:
                System.out.println("[WARN]:" + message);
                break;
            case ERROR:
                System.err.println("[ERROR]:" + message);
                break;
            case TRACE:
                System.out.println("... " + message);
                break;
        }
    }

}
