package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.ReportEvaluator;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReportInstantFeedback;

public class ReportEvaluatorByScoreRangeImpl implements ReportEvaluator {

    private static final int LOW_LEVEL_UPPER_BORDER = 6;
    private static final int HIGH_LEVEL_UPPER_BORDER = 10;


    @Override
    public QuestionnaireReportInstantFeedback buildFeedback(QuestionnaireReport report) {
        if (report.getFinalScore() <= LOW_LEVEL_UPPER_BORDER) {
            return new QuestionnaireReportInstantFeedback(false, "Unfortunately, we don’t match");
        } else if (report.getFinalScore() < HIGH_LEVEL_UPPER_BORDER){
            return new QuestionnaireReportInstantFeedback(true, "That looks good!");
        } else {
            return new QuestionnaireReportInstantFeedback(true, "Excellent!");
        }
    }
}
