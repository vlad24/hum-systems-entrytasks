package com.humsystems.entrytasks.questionnaire.app.component;

public interface ConfigurationsProvider {

    String getStringProperty(String name);

    Integer getIntegerProperty(String name);

    Double getDoubleProperty(String name);


    enum PREDEFINED_CONFIGS {
        REPORT_SEND_URL("url.send_report"),
        REPORT_SEND_TIMEOUT("timeout.sec.send_report")
        ;

        private final String propKey;


        PREDEFINED_CONFIGS(String propKey) {

            this.propKey = propKey;
        }


        public String getPropKey() {
            return propKey;
        }
    }
}
