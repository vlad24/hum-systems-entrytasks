package com.humsystems.entrytasks.questionnaire.app;

import com.humsystems.entrytasks.questionnaire.app.component.*;

/**
 * Application to show questionnaire to user interact with it
 */
public abstract class QuestionnaireApp {

    protected final Logger appLogger;
    protected final ReportSender reportSender;
    protected final ConfigurationsProvider configurationsProvider;
    protected final ReportEvaluator reportEvaluator;


    protected QuestionnaireApp() {
        reportSender = getComponentInitializerFactory().getReportSender();
        appLogger = getComponentInitializerFactory().getAppLogger();
        configurationsProvider = getComponentInitializerFactory().getConfigurationProvider();
        reportEvaluator = getComponentInitializerFactory().getReportConfigurator();
    }

    protected abstract ComponentFactory getComponentInitializerFactory();

    public abstract void run();

}
