package com.humsystems.entrytasks.questionnaire.app.component;

import com.humsystems.entrytasks.questionnaire.domain.Question;
import com.humsystems.entrytasks.questionnaire.domain.Questionnaire;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

/**
 * Entity representing questionee's set of answers or some questionnaire and navigation over it
 */
public abstract class QuestionnaireAnswerer {

    protected final Questionnaire questionnaire;

    /**
     * Constructs bidirectional navigator over the provided questionnaire
     * @param questionnaire questionnaire the built navigator will navigate through
     */
    public QuestionnaireAnswerer(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    /**
     * Starts the navigation session over the questionnaire
     */
    public abstract void openAnswerSession();


    /**
     * Ensures the answerer is in the right state and delegates to {@link #fetchCurrentQuestionNumber()}.
     * @throws IllegalStateException if the answerer is not opened (see {@link #isCurrentQuestionAlreadyAnswered()} ()})
     */
    public int getCurrentQuestionNumber(){
        assertOpened();
        return fetchCurrentQuestionNumber();
    }

    /**
     * Gets the number of the currently observed question starting from zero
     * @return number of the currently observed question
     */
    protected abstract int fetchCurrentQuestionNumber();

    /**
     * Gets amount of questions that can be displayed by the navigator using underlying questionnaire
     * @return total amount of questions of underlying questionnaire
     */
    public int getAllQuestionAmount() {
        return questionnaire.getQuestions().size();
    }

    /**
     * Ensures the answerer is opened and delegates to {@link #fetchNext()}.
     * @throws IllegalStateException if the answerer is not opened (see {@link #isCurrentQuestionAlreadyAnswered()} ()})
     */
    public Optional<Question> getNext(){
        assertOpened();
        return fetchNext();
    }

    /**
     * Fetches the next question in the questionnaire or empty optional if there is no subsequent question. <br/>
     * @return optional containing the next question or empty if the current observed question is the last one in the questionnaire
     */
    protected abstract Optional<Question> fetchNext();

    /**
     * Ensures the answerer is opened and delegates to {@link #fetchPrevious()}.
     * @throws IllegalStateException if the answerer is not opened (see {@link #isCurrentQuestionAlreadyAnswered()} ()})
     */
    public Optional<Question> getPrevious(){
        assertOpened();
        return fetchPrevious();
    }

    /**
     * Gets the previous question in the questionnaire or empty optional if there is no previous question. <br/>
     * @return optional containing the previous question or empty if the current observed question is the first one in the questionnaire
     */
    protected abstract Optional<Question> fetchPrevious();

    /**
     * Ensures the answerer is opened and delegates to {@link #answerCurrentlyOpenedQuestion(Set)}.
     * @throws IllegalStateException if the answerer is not opened (see {@link #isCurrentQuestionAlreadyAnswered()})
     * @throws NoSuchElementException if there is no current observed question
     * @throws IllegalArgumentException if the question is single choice one, but multiple answers are provided
     */
    public void answerCurrent(Set<Integer> answerPositions){
        assertHasCurrent();
        assertAnswersAreConsistentWithQuestionType(answerPositions);
        answerCurrentlyOpenedQuestion(answerPositions);
    }


    /**
     * Ensures the answerer is opened and delegates to {@link #fetchCurrent()}.
     * @throws IllegalStateException if the answerer is not opened (see {@link #isCurrentQuestionAlreadyAnswered()} ()})
     */
    public Optional<Question> getCurrent(){
        assertOpened();
        return fetchCurrent();
    }
    /**
     * Gets currently opened question. The answerer is guaranteed to be opened when the method call is performed
     * @return currently observed question
     */
    protected abstract Optional<Question> fetchCurrent();


    /**
     * Stores the positions which were chosen by the user for currently observed open question.<br/>
     * The answerer is guaranteed to be opened and have currently opened question
     * @param answerPositions positions which
     */
    protected abstract void answerCurrentlyOpenedQuestion(Set<Integer> answerPositions);


    /**
     * Ensures the answerer is opened and if correct question number is provided delegates to {@link #fetchProvidedAnswers(int)}.
     * Otherwise returns empty optional
     * @param questionNumber number of question
     * @return positions of answers provided by the user
     */
    public Optional<Set<Integer>> getProvidedAnswers(int questionNumber){
        assertOpened();
        if (questionNumber < 0 || questionNumber >= getAllQuestionAmount()){
            return Optional.empty();
        } else {
            return fetchProvidedAnswers(questionNumber);
        }
    }


    /**
     * Fetches answers the user has already submitted. If the question was not answered empty optional is returned.
     * Question number is guaranteed to be in range [0, {@link #getAllQuestionAmount()}-value]
     * @return optional set of the answer positions
     */
    protected abstract Optional<Set<Integer>> fetchProvidedAnswers(int questionPosition);


    /**
     * Shows whether currently answered question is actually answered. If the answerer is not opened no current question is there and the result is false
     * @return true if the current question has been already answered
     */
    public abstract boolean isCurrentQuestionAlreadyAnswered();

    /**
     * Closes the current answer session (indicating that the user is done with the questionnaire) and computes the questionnaire report
     * @return questionnaire report based on the user's answers
     */
    public abstract QuestionnaireReport closeAnswerSession();

    /**
     * Returns if the answer session is currently opened
     * @return if the answer session is currently opened
     */
    public abstract boolean isAnswerSessionOpened();



    private void assertHasCurrent() {
        fetchCurrent().orElseThrow(() -> new NoSuchElementException("There is no currently observed question"));
    }


    private void assertAnswersAreConsistentWithQuestionType(Set<Integer> answerPositions) {
        Optional<Question> current = fetchCurrent();
        if (current.isPresent() && !current.get().isMultipleChoice() && answerPositions.size() > 1) {
            throw new IllegalArgumentException("Cannot answer single choice questions with multiple answers");
        }
    }

    private void assertOpened() {
        if (!isAnswerSessionOpened()){
            throw new IllegalStateException("Open answerer first");
        }
    }
}
