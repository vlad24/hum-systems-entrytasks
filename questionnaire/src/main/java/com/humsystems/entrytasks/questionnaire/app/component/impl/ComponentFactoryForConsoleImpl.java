package com.humsystems.entrytasks.questionnaire.app.component.impl;

import com.humsystems.entrytasks.questionnaire.app.component.*;
import com.humsystems.entrytasks.questionnaire.domain.Questionnaire;

public class ComponentFactoryForConsoleImpl implements ComponentFactory {


    @Override
    public QuestionnaireAnswerer getQuestionnaireNavigator(String questionee, Questionnaire questionnaire) {
        return new QuestionnaireAnswererBasicImpl(questionee, questionnaire);
    }


    @Override
    public Logger getAppLogger() {
        return new LoggerConsoleImpl();
    }


    @Override
    public ReportSender getReportSender() {
        return new ReportSenderDummyImpl();
    }


    @Override
    public ConfigurationsProvider getConfigurationProvider() {
        return new ConfigurationsProviderDummyImpl();
    }


    @Override
    public ReportEvaluator getReportConfigurator() {
        return new ReportEvaluatorByScoreRangeImpl();
    }
}
