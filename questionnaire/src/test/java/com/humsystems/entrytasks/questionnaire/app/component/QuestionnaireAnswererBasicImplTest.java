package com.humsystems.entrytasks.questionnaire.app.component;

import com.humsystems.entrytasks.questionnaire.app.component.impl.QuestionnaireAnswererBasicImpl;
import com.humsystems.entrytasks.questionnaire.domain.Answer;
import com.humsystems.entrytasks.questionnaire.domain.Question;
import com.humsystems.entrytasks.questionnaire.domain.Questionnaire;
import com.humsystems.entrytasks.questionnaire.domain.QuestionnaireReport;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

public class QuestionnaireAnswererBasicImplTest {

    private final static String MOCK_QUESTIONNAIRE_NAME = "Test";
    private final static String MOCK_CANDIDATE_NAME = "Candidate";
    private final static List<Question> MOCK_SINGLE_CHOICE_QUESTIONS = Arrays.asList(
            new Question("Q0", Arrays.asList(new Answer("Q0A0", 6), new Answer("Q0A1", 1), new Answer("Q0A2", 2)), false),
            new Question("Q1", Arrays.asList(new Answer("Q1A0", 1), new Answer("Q1A1", 6), new Answer("Q1A2", 3)), false),
            new Question("Q2", Arrays.asList(new Answer("Q2A0", 2), new Answer("Q2A1", 3), new Answer("Q2A2", 6)), true)
    );

    private QuestionnaireAnswererBasicImpl answerer;


    @Before
    public void setUp() {
        Questionnaire questionnaire = new Questionnaire(MOCK_QUESTIONNAIRE_NAME, MOCK_SINGLE_CHOICE_QUESTIONS);
        answerer = new QuestionnaireAnswererBasicImpl(MOCK_CANDIDATE_NAME, questionnaire);
    }


    @Test
    public void startAnswerSession_WhenStarted_ThenSessionOpened() {
        answerer.openAnswerSession();
        Assert.assertTrue("Session is not opened after start", answerer.isAnswerSessionOpened());
    }


    @Test
    public void getCurrentQuestionNumber_WhenStarted_ThenQuestionNumberIsNegative() {
        answerer.openAnswerSession();
        Assert.assertEquals(-1, answerer.getCurrentQuestionNumber());
    }


    @Test(expected = IllegalStateException.class)
    public void getCurrentQuestionNumber_WhenClosed_ThenQuestionNumberIsUnavailable() {
        answerer.getCurrentQuestionNumber();
    }


    @Test
    public void getAllQuestionAmount_WhenStarted_ThenQuestionNumberIsAccessible() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        Assert.assertEquals(MOCK_SINGLE_CHOICE_QUESTIONS.size(), answerer.getAllQuestionAmount());
    }


    @Test
    public void getAllQuestionAmount_WhenClosed_ThenQuestionAmountIsAccessible() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        Assert.assertEquals(MOCK_SINGLE_CHOICE_QUESTIONS.size(), answerer.getAllQuestionAmount());
    }

    @Test
    public void getNext_WhenJustOpened_ThenNextCallGetsCurrentPositionEqualZero() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        answerer.getNext();
        Assert.assertEquals("Next returned not first element!", 0, answerer.getCurrentQuestionNumber());
    }

    @Test
    public void getNext_WhenJustOpened_ThenNextCallGetsFirstQuestion() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        Optional<Question> next = answerer.getNext();
        Assert.assertTrue("Next is empty!", next.isPresent());
        Assert.assertEquals("Next returned not first element!", MOCK_SINGLE_CHOICE_QUESTIONS.get(0), next.get());
    }

    @Test
    public void getNext_WhenTooManyNextCallsIssued_ThenAnswererReturnsEmpty() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        final int tooManyCalls = (1 + MOCK_SINGLE_CHOICE_QUESTIONS.size()) * 2;
        //noinspection OptionalAssignedToNull - just to ensure getNext actually does something
        Optional<Question> lastQuestion = null;
        for (int i = 0; i < tooManyCalls; i++) {
            lastQuestion = answerer.getNext();
        }
        Assert.assertEquals("Non empty is optional is finally obtained", Optional.empty(), lastQuestion);
    }


    @Test
    public void getNext_WhenTooManyNextCallsIssued_ThenAnswererPositionIsQuestionListSize() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        final int tooManyCalls = (1 + MOCK_SINGLE_CHOICE_QUESTIONS.size()) * 2;
        //noinspection OptionalAssignedToNull - just to ensure getNext actually does something
        Optional<Question> lastQuestion = null;
        IntStream.range(0, tooManyCalls).forEach(i -> answerer.getNext());
        Assert.assertEquals("Position is incorrect (not equals size)", MOCK_SINGLE_CHOICE_QUESTIONS.size(), answerer.getCurrentQuestionNumber());
    }


    @Test
    public void getPrevious_WhenInvokedAfterNext_ThenReturnsTheOneNextStoodAt() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        Optional<Question> initialElement = answerer.getNext();
        answerer.getNext();
        Optional<Question> elementAfterPreviousCall = answerer.getPrevious();
        Assert.assertEquals("Next + Previous != ZeroAction", initialElement, elementAfterPreviousCall);
    }


    @Test
    public void getNext_WhenStandAtTheEndAndPreviousCalled_ThenReturnedOneBeforeLast() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        IntStream.range(0, MOCK_SINGLE_CHOICE_QUESTIONS.size()).forEach(i -> answerer.getNext());
        Optional<Question> oneBeforeLast = answerer.getPrevious();
        Assert.assertEquals("Not one before last is returned",
                Optional.ofNullable(MOCK_SINGLE_CHOICE_QUESTIONS.size() == 1 ? null : MOCK_SINGLE_CHOICE_QUESTIONS.get(MOCK_SINGLE_CHOICE_QUESTIONS.size() - 2)),
                oneBeforeLast
        );
    }

    @Test
    public void answerCurrent_WhenQuestionIsAnswered_ThenTheAnswersIsCorrectlyStored() {
        answerer.openAnswerSession();
        answerer.getNext();
        Set<Integer> answerPositions = new HashSet<>(Collections.singletonList(1));
        answerer.answerCurrent(answerPositions);
        Optional<Set<Integer>> providedAnswers = answerer.getProvidedAnswers(0);
        Assert.assertTrue(providedAnswers.isPresent());
        Assert.assertEquals(answerPositions, providedAnswers.get());
    }

    @Test
    public void answerCurrent_WhenQuestionIsAnsweredAndQuestionPositionMoves_ThenTheAnswersIsCorrectlyStoredAnyway() {
        answerer.openAnswerSession();
        answerer.getNext();
        Set<Integer> answerPositions = new HashSet<>(Collections.singletonList(2));
        answerer.answerCurrent(answerPositions);
        answerer.getNext();
        Optional<Set<Integer>> providedAnswers = answerer.getProvidedAnswers(0);
        Assert.assertTrue("Empty answers got", providedAnswers.isPresent());
        Assert.assertEquals("Not expected answers got", answerPositions, providedAnswers.get());
    }

    @Test
    public void answerCurrent_WhenQuestionIsAnsweredManyTimes_ThenTheLastAnswerIsCorrectlyStored() {
        answerer.openAnswerSession();
        answerer.getNext();
        Set<Integer> answerPositionsFirst = new HashSet<>(Collections.singletonList(1));
        Set<Integer> answerPositionsSecond = new HashSet<>(Collections.singletonList(0));
        answerer.answerCurrent(answerPositionsFirst);
        answerer.answerCurrent(answerPositionsSecond);
        Optional<Set<Integer>> providedAnswers = answerer.getProvidedAnswers(0);
        Assert.assertTrue("Empty answers got", providedAnswers.isPresent());
        Assert.assertEquals("Not expected answers got", answerPositionsSecond, providedAnswers.get());
    }

    @Test(expected = NoSuchElementException.class)
    public void answerCurrent_WhenNoQuestionIsActuallyObserved_ThenAnsweringProhibited() {
        answerer.openAnswerSession();
        Set<Integer> answerPositions = new HashSet<>(Arrays.asList(0, 1));
        answerer.answerCurrent(answerPositions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void answerCurrent_WhenQuestionIsSingleChoice_ThenMultipleAnswersAreNotAccepted() {
        answerer.openAnswerSession();
        answerer.getNext();
        Set<Integer> answerPositions = new HashSet<>(Arrays.asList(1, 2));
        answerer.answerCurrent(answerPositions);
    }


    @Test
    public void isCurrentQuestionAlreadyAnswered_WhenAnswered_ThenTheMethodReturnsTrue() {
        answerer.openAnswerSession();
        answerer.getNext();
        Set<Integer> answerPositions = new HashSet<>(Collections.singletonList(1));
        answerer.answerCurrent(answerPositions);
        Assert.assertTrue("Question answers are not accounted", answerer.isCurrentQuestionAlreadyAnswered());
    }

    @Test
    public void isCurrentQuestionAlreadyAnswered_WhenNotAnswered_ThenTheMethodReturnsFalse() {
        answerer.openAnswerSession();
        answerer.getNext();
        Assert.assertTrue("Question answers are accounted in a phantom-way", !answerer.isCurrentQuestionAlreadyAnswered());
    }


    @Test
    public void closeAnswerSession_WhenClosed_ThenSessionIsClosed() {
        answerer.openAnswerSession();
        answerer.closeAnswerSession();
        Assert.assertTrue("Session is not closed after close", !answerer.isAnswerSessionOpened());
    }

    @Test
    public void closeAnswerSession_WhenClosedAfterAnsweringForXSec_ThenReportContainsCorrectDurationGeqXSec() {
        final int secondsToAnswer = 2;
        answerer.openAnswerSession();
        try{
            Thread.sleep(secondsToAnswer * 1000);
        } catch (InterruptedException ignored) {}
        QuestionnaireReport report = answerer.closeAnswerSession();
        Assert.assertTrue("Not correct duration is got", report.getDuration().getSeconds() >= secondsToAnswer);
    }

    @Test
    public void closeAnswerSession_WhenClosedAfterAnswering_ThenReportContainsCorrectQuestionee() {
        answerer.openAnswerSession();
        QuestionnaireReport report = answerer.closeAnswerSession();
        Assert.assertEquals("Not correct questionee is included to the report", report.getQuestionee(), MOCK_CANDIDATE_NAME);
    }

    @Test
    public void closeAnswerSession_WhenClosedAfterAnswering_ThenFinalScoreIsCorrect_1() {
        final int expectedScore = 6 * 3;
        answerer.openAnswerSession();
        IntStream.range(0, answerer.getAllQuestionAmount()).forEach(i -> {
            answerer.getNext();
            answerer.answerCurrent(Collections.singleton(i)); // max score based on MOCK_SINGLE_CHOICE_QUESTIONS
        });
        QuestionnaireReport report = answerer.closeAnswerSession();
        Assert.assertEquals("Not correct score is calculated", expectedScore, report.getFinalScore());
    }

    @Test
    public void closeAnswerSession_WhenClosedAfterAnswering_ThenFinalScoreIsCorrect_2() {
        final int expectedScore = 16;
        answerer.openAnswerSession();
        int allQuestionAmount = answerer.getAllQuestionAmount();
        for (int i = 0; i < allQuestionAmount; i++) {
            answerer.getNext();
            if (i == allQuestionAmount - 1){
                answerer.answerCurrent(new HashSet<>(Arrays.asList(1, 2, 3)));
            } else {
                answerer.answerCurrent(Collections.singleton(0));
            }
        }
        QuestionnaireReport report = answerer.closeAnswerSession();
        Assert.assertEquals("Not correct score is calculated", expectedScore, report.getFinalScore());
    }

    @Test
    public void getCurrent_WhenJustOpened_ThenCurrentIsEmpty() {
        answerer.openAnswerSession();
        Assert.assertFalse(answerer.getCurrent().isPresent());
    }

    @Test
    public void getCurrent_WhenNextIsInvoked_ThenCurrentIsUpdated() {
        answerer.openAnswerSession();
        Optional<Question> next = answerer.getNext();
        Assert.assertEquals(next, answerer.getCurrent());
    }

    @Test(expected = IllegalStateException.class)
    public void getCurrent_WhenClosed_ThenCurrentIsInaccessible() {
        answerer.getCurrent();
    }

    @Test
    public void getCurrent_WhenAdvancedTooFar_ThenCurrentIsEmpty() {
        Assume.assumeFalse("It is assumed that mock questions is not empty", MOCK_SINGLE_CHOICE_QUESTIONS.isEmpty());
        answerer.openAnswerSession();
        final int tooManyCalls = (1 + MOCK_SINGLE_CHOICE_QUESTIONS.size()) * 2;
        IntStream.range(0, tooManyCalls).forEach(i -> answerer.getNext());
        Assert.assertEquals("Current is not empty after hitting the right limit", Optional.empty(), answerer.getCurrent());
    }

}